EAPI=7

inherit meson

DESCRIPTION="Launches programs in another network namespace without root"
HOMEPAGE="https://codeberg.org/cirho/nslaunch"
SRC_URI="https://codeberg.org/cirho/nslaunch/archive/v${PV}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/${PN}"

LICENSE="MIT"

SLOT="0"

KEYWORDS="~amd64"
